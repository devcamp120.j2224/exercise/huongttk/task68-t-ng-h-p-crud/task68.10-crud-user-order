package com.user_order.useroderapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.useroderapi.models.COrder;
import com.user_order.useroderapi.repository.IOrderRepository;
import com.user_order.useroderapi.service.COrderService;

@RestController
@CrossOrigin
@RequestMapping("/orders")
public class COrderController {
    @Autowired
    private COrderService orderService;
    @Autowired
    IOrderRepository iOrderRepository;

    //--------------------GET--------------
    @GetMapping
    public ResponseEntity<List<COrder>> getAllOrder(){
        ResponseEntity<List<COrder>> respon = orderService.getAllOrder();
        return respon;
    }
    @GetMapping("{id}")
    public ResponseEntity <Object> getOrderById(@PathVariable("id") long id) {
        ResponseEntity <Object> respon = orderService.getOrderById(id);
        return respon;
    }
    //GET ORDER BY USERID
   
	@GetMapping("/userId/{userId}")
    public List < COrder > getRegionsByCountry(@PathVariable(value = "userId") Long userId) {
        return iOrderRepository.findByUserId(userId);
    }


    //--------------POST------------
    @PostMapping
	public ResponseEntity<Object> createRegion(@RequestParam(value = "userId") Long userId,@RequestBody COrder order) {
		ResponseEntity<Object> respon = orderService.createOrder(userId, order);
		return respon;
	}


    //--------------PUT---------------
    @PutMapping("{id}")
    public ResponseEntity <Object> putOrder(@PathVariable long id, @RequestBody COrder order) {
        ResponseEntity <Object> respon = orderService.updateOrderById(id, order);
        return respon;
    }

    //--------------DELETE-----------------
   
    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
        ResponseEntity<Object> respon = orderService.deleteOrderById(id);
        return respon;
    }
    
    @DeleteMapping()
    public ResponseEntity<Object> deleteAllUsers() {
        ResponseEntity<Object> respon = orderService.deleteOrder();
        return respon;
    }
}
