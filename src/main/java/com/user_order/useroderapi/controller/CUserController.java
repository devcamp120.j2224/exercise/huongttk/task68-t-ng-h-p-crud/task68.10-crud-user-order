package com.user_order.useroderapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.useroderapi.models.CUser;
import com.user_order.useroderapi.service.CUserService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class CUserController {
    @Autowired
    private CUserService userService;

    //--------------------GET--------------
    @GetMapping
    public ResponseEntity<List<CUser>> getAllUsers(){
        ResponseEntity<List<CUser>> respon = userService.getAllUser();
        return respon;
    }
    @GetMapping("{id}")
    public ResponseEntity <Object> getUserById(@PathVariable("id") long id) {
        ResponseEntity <Object> respon = userService.getUserById(id);
        return respon;
    }


    //--------------POST------------
    @PostMapping
    public ResponseEntity <Object> postUser(@RequestBody CUser user) {
        ResponseEntity <Object> responUser = userService.createUser(user);
        return responUser;
    }


    //--------------PUT---------------
    @PutMapping("{id}")
    public ResponseEntity <Object> putUser(@PathVariable long id, @RequestBody CUser user) {
        ResponseEntity <Object> respon = userService.updateUserById(id, user);
        return respon;
    }

    //--------------DELETE-----------------
   
    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
        ResponseEntity<Object> responseUsers = userService.deleteUserById(id);
        return responseUsers;
    }
    
    @DeleteMapping()
    public ResponseEntity<Object> deleteAllUsers() {
        ResponseEntity<Object> responseUsers = userService.deleteUsers();
        return responseUsers;
    }

}
