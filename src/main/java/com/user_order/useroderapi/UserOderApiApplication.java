package com.user_order.useroderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserOderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserOderApiApplication.class, args);
	}

}
