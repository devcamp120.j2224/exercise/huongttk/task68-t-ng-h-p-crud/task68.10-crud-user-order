package com.user_order.useroderapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user_order.useroderapi.models.COrder;


@Repository
public interface IOrderRepository extends JpaRepository <COrder, Long>{
  
    List< COrder> findByUserId(Long userId);
}
